﻿# Deploying and Configuring Web Connection Applications

So, you've built your shiny new Web Connection Application on your local machine for development and you're proud of what you've accomplished. Now it's time to take that local masterpiece and get it online. How you do this?

In this article I'll take you to the process of creating a small placehold application and deploy it to a live server on a hosted service called Vultr which provides low cost, and high performance virtual machine services that are ideal of hosting a Web Connection site.

## Creating new Applications - Projects Organize your Application
Starting with Web Connection 6.0 the process of creating a new application has been made much more consistent through a way of organizing a project into a well-known folder structure. The idea is to recognize what most projects are made up out of which is:

* Code and/or Binary files
* Web content
* Data

The Web Connection new project structure create a single top level folder, with subfolders for `Deploy` (code and binaries), Web and Data. The `Deploy` folder then also contains the `Temp` folder where message files and logs are stored.

![](WebConnectionProjectLayout.png)

Using this structure provides a simple hierarchy that is known at runtime and can be referenced using **relative paths**. The code in the application is running out of the `Deploy` folder knows that it can get to the `Web` folder simply by using a relative path `..\Web`.

This allows for the tooling to make some assumptions when configuring Web sites and configuration files which can now reliably reference these relative paths. Moreover because the paths are relative we can move the top level folder to a new location and simply change the base Web configuration - typically the Virtual directory - and have everything else just work as is.

