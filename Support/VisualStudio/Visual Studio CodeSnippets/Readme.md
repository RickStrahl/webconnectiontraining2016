# Web Connection HTML Snippets for Visual Studio

These snippets in this folder are useful Web Connection and Bootstrap 4 snippets that help with Web Connection specific layout and settings.

To install:

* Unzip this folder
* Copy it into  

```txt
%userprofile%\Documents\Visual Studio 2017\Code Snippets\Visual Web Developer\My HTML Snippets\Web Connection
```
