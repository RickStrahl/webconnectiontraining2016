var app = angular.module('app', [
// 'ngRoute',
// 'ngSanitize',
// 'ngAnimate'
    ]);

// Add Routing
// app.config(['$routeProvider',
//      function($routeProvider) {
//           $routerProvider
//             .when("/albums", {
//                 templateUrl: "app/views/albums.html",
//             })
//             .otherwise({
//                 redirectTo: "/albums"
//             });
//      }
// ]);

app.controller('pageController', pageController);

pageController.$inject = ['$scope','$http','$timeout'];

function pageController($scope, $http, $timeout) {

    var vm = this;

    // Interface
    vm.message = "Hello World from AngularJs. JavaScript time is: "  + new Date();
    vm.buttonClick = buttonClick;

    // Initialization
    initialize();

    return;

    // Interface function implementations

    function initialize() {
        console.log("page controller started.");
    }

    function buttonClick()  {
        vm.message = "Curious little bugger are you? Good! Updated time is: " + new Date();
    }
}
