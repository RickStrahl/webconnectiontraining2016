﻿# Web Connection Training 2018
### Materials, Notes and Support Materials

Thank you for attending the Training event at Southwest Fox. This repository contains all the Slides and documents for the sessions at the conference. This year's conference is a bit different than others in that it's mostly about code, so there are less support materials than usual.

But we also provide a host of features like Visual Studio Intellisense Snippets, FoxCode Intellisense snippets, and links to separate source code repositories for the two main sample applications we will build and talk about at the conference.

### Online Samples
The following are the fully built sample applications we worked on:

* **[TimeTrakker Server Side Sample](http://TimeTrakkerswf.west-wind.com)**  
![](http://chart.apis.google.com/chart?cht=qr&chl=http%3A%2F%2Ftimetrakkerswf.west-wind.com%2F&chs=120x120)

* **[AlbumViewer Client Side Mobile Sample](http://albumviewer2swf.west-wind.com/)**  
![](http://chart.apis.google.com/chart?cht=qr&chl=http%3A%2F%2Falbumviewer2swf.west-wind.com%2F%23%2Falbum%2F516&chs=120x120)

<small>*You can use the QR barcode to scan the URL on your mobile device*</small>

### Sample Source Code
The source code for the completed samples that we'll be working on during the conference are available at the following separate BitBucket repositories:

* **[TimeTrakker Server Side MVC Sample App](https://bitbucket.org/RickStrahl/southwestfoxtimetrakker)**
* **[AlbumViewer Client Side Angular/Mobile Sample App](https://bitbucket.org/RickStrahl/southwestfoxalbumviewer2)**

You can clone these samples into a folder and run with only minor modifications to the configuration. Specifically you'll need to fix your FoxPro path to ensure that the Web Connection `classes` and install folder can be found (either in web.config, SETPATHS.PRG or explicit `SET PATH` statements).

### Session Notes and Slides
The [Content](https://bitbucket.org/RickStrahl/webconnectiontraining2016/src/772d6bbd3572c607eafed8488d214914e1790177/Content/?at=master) folder contains all of the PowerPoint slides from the conference as well as any support whitepapers. If you're viewing the content online, you can browse to the documents and use the `Raw` view to see the actual document content displayed either in a browser viewer or downloaded and opened locally. Most papers are in Word document format and the slides use PowerPoint.

### Updates
I will be updating this example **before, during and after** the conference, so if you want to grab the files and locally use them it's probably a good idea to use Git to clone this repository, so you can later pull updates.

### Downloading Content from BitBucket
Cloning with Git is the best option as it allows you to keep updated with changes from the site.

But you can also download materials in any of the BitBucket repositories by clicking the Downloads button in the left section of the repo. You can switch branches and access specific tags to downloads specific versions/branches as needed. We'll be using branches for the code projects we work on during the conference.

### Git Clients
To help work with Git you might want to use a Git client other than the command line. The command line always works (once Git is installed), but personally I like to use a more interactive client:

* [TortoiseGit](https://tortoisegit.org/)  (for Windows Shell Integration in Explorer)
* [SourceTree](https://www.sourcetreeapp.com/)   (a nice and free Git UI) 

I use both of these on a regular basis but if you only use one I recommend TortoiseGit as it's easiest to get up and running with.